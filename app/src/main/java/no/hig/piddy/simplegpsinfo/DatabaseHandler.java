package no.hig.piddy.simplegpsinfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Class for handling database actions. It uses SQLite.
 * <a href="http://www.javatpoint.com/android-sqlite-tutorial">This</a> tutorial was used
 * for creating this code.
 * @author Per-Kristian Nilsen, 130397
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    public DatabaseHandler(Context context) {
        super(context, "gpsRecordManager", null, 1);
    }

    /**
     * This method is called when an instance of the DatabaseHandler is created.
     * It creates the records table, and inserts one placeholder row. This one row is the
     * only one in use. The row is only ever updated.
     * @param db the database instance
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_RECORDS_TABLE = "CREATE TABLE records(id INTEGER PRIMARY KEY, lat DOUBLE, "+
                "lng DOUBLE, city TEXT, temp TEXT);";
        db.execSQL(CREATE_RECORDS_TABLE);

        //Add empty placeholder record
        ContentValues values = new ContentValues();
        values.put("id", 1);
        values.put("lat", 0);
        values.put("lng", 0);
        values.put("city", "nowhere");
        values.put("temp", "0");

        // Inserting Row
        db.insert("records", null, values);
        //db.close();

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + "records");

        // Create tables again
        onCreate(db);
    }

    /**
     * This is the method for updating the only row in the table.
     * @param lat the latitude
     * @param lng the longitude
     * @param city the city name
     * @param temp the temperature in celsius
     * @return a confirmation of the update
     */
    public int updateRecord(double lat, double lng, String city, String temp) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("lat", lat);
        values.put("lng", lng);
        values.put("city", city);
        values.put("temp", temp);

        // updating row
        return db.update("records", values, "id" + " = ?",
                new String[] { "1" });
    }

    /**
     * This method gets the city and temperature from the only record in the table, and
     * returns it.
     * @return the record
     */
    String[] getRecord() {
        SQLiteDatabase db = this.getReadableDatabase();

        // Builds the SQL-query
        Cursor cursor = db.query("records", new String[] { "city", "temp" }, "id" + "=?",
                new String[] { String.valueOf(1) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        // Gets the values
        String[] record = new String[2];
        record[0] = cursor.getString(0);
        record[1] = cursor.getString(1);
        // return city and temp
        return record;
    }
}
