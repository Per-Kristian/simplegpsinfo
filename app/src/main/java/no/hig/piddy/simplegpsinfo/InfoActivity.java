package no.hig.piddy.simplegpsinfo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;


/**
 * This class represents the activity cointaining information about the location and weather.
 * It also shows where the user was the last time this activity was launched. The information
 * is fetched from in another thread, so before it is fetched, the activity will tell the user
 * that the information is loading.
 * @author Per-Kristian Nilsen, 130397
 */
public class InfoActivity extends AppCompatActivity {

    private double latitude;
    private double longitude;
    private int altitude;
    private String city;
    Context activityContext = this;
    Location location;

    /**
     * This is a subclass for downloading information in a separate thread.
     */

    private class DownloadInfoTask extends AsyncTask<Void, Void, Void> {

        String[] info = new String[4]; // For passing information
        Drawable bitDraw;

        /**
         * Starts the downloading in a separate thread. Must be done, since networking in
         * UI-thread is not allowed.
         * @param params void
         * @return
         */
        protected Void doInBackground(Void... params){

            try{
                // Creates a geocoder and gets information based on coordinates.
                Geocoder geocoder = new Geocoder(activityContext, Locale.getDefault());
                List <Address>addresses = geocoder.getFromLocation(latitude, longitude, 1);

                // Gets the name of the city.
                city = addresses.get(0).getLocality();
                info[0] = city;

                // Gets the weather
                String[] weatherDesc = getWeather(latitude, longitude);
                info[1] = weatherDesc[0]; //Index 0 contains the description
                info[2] = weatherDesc[1]; //Index 1 contains the icon-code
                info[3] = weatherDesc[2]; //Index 2 contains the temperature

                // Downloads weather icon
                bitDraw = downloadWeatherImage();

            } catch (Exception e){
                String TAG = "";
                Log.e(TAG, Log.getStackTraceString(e));
            }
            return null;
        }

        /**
         * This method specifies what should be done after the separate thread for
         * downloading info has finished it's work. The method fills the textviews with
         * information, and the imageview with an icon. It also updates the database with the
         * new location.
         * @param result void
         */
        protected void onPostExecute(Void result){
            TextView textCity = (TextView) findViewById(R.id.textCity);
            TextView textWeather = (TextView) findViewById(R.id.textWeather);
            TextView textLastLocation = (TextView) findViewById(R.id.textLastLocation);
            ImageView iconWeather = (ImageView) findViewById(R.id.iconWeather);

            textCity.setText("Looks like you're in " + info[0] + ", currently " + altitude +
                    " meters above the ocean, and the temperature is " + info[3] +
                    " degrees celsius.");
            textWeather.setText(info[1]);
            iconWeather.setImageDrawable(bitDraw);

            // Reads the database and displays the last location.
            DatabaseHandler db = new DatabaseHandler(activityContext);
            String[] lastRecord = db.getRecord();
            textLastLocation.setText("Last time you checked, you were in " + lastRecord[0] +
                    ", and the temperature was " + lastRecord[1] + " degrees.");

            // Updates the database with the latest location.
            db.updateRecord(latitude, longitude, info[0], info[3]);
        }

        /**
         * This is a method for downloading the weather-icon. The way of doing this was inspired
         * by Entreco's answer to
         * <ahref="http://stackoverflow.com/questions/8423987/
         * download-image-for-imageview-on-android">this</a> Stackoverflow question.
         * @return
         */
        private Drawable downloadWeatherImage() {

            String urlString = "http://openweathermap.org/img/w/" + info[2] + ".png";
            try {
                // Creates a connection to the URL
                URL iconUrl = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) iconUrl.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.connect();

                // Gets the content of the URL and decodes it
                InputStream inputStream;
                inputStream = connection.getInputStream();
                BufferedInputStream buf = new BufferedInputStream(inputStream);
                Bitmap bitmap = BitmapFactory.decodeStream(buf);

                return new BitmapDrawable(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * This method specifies what to do when the infoactivity is launched.
     * It gets the location from the intent, and saves the latitude, longitude and altitude.
     * It then starts the separate thread for downloading information.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // Gets the location from the intent
        Intent infoIntent = getIntent();
        location = infoIntent.getExtras().getParcelable(MapsActivity.INFO_MESSAGE);

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            // Gets altitude, rounds it and casts it to an integer
            double altitudeRaw = location.getAltitude();
            altitude = (int) Math.round(altitudeRaw);

            // Starts the downloader thread
            new DownloadInfoTask().execute();
        } else{
            // Tell the user to update first.
            TextView textCity = (TextView) findViewById(R.id.textCity);
            textCity.setText("Press update on the map before checking the info!");

            TextView textWeather = (TextView) findViewById(R.id.textWeather);
            textWeather.setText("");
        }
    }

    /**
     * This method does the actual work of downloading weather information. It is called from
     * the doInBackground method. It uses the OpenWeatherMap.org API.
     * @param lat the latitude
     * @param lng the longitude
     * @return a string array with information about the weather
     */
    public String[] getWeather(double lat, double lng){
        InputStream inputStream;
        try{
            // Gets the weather data from the URL
            String urlString = "http://api.openweathermap.org/data/2.5/weather?lat=" +lat+
                    "&lon=" +lng + "&APPID=dda604e24caf4e8228923464f48a5cb2";
            URL weatherUrl = new URL(urlString) ;
            HttpURLConnection connection = (HttpURLConnection) weatherUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();

            // Reads the data and inserts into a string
            inputStream = connection.getInputStream();
            String jsonAsString = readStream(inputStream);

            if (inputStream != null){
                inputStream.close();
            }

            // Creates a JSONObject from the string, and digs into it to find various information
            JSONObject result = new JSONObject(jsonAsString);

            JSONArray weatherArray = new JSONArray();
            weatherArray.put(result.getJSONArray("weather"));
            JSONArray weatherObject = weatherArray.getJSONArray(0);
            JSONObject weatherObject2 = weatherObject.getJSONObject(0);

            String[] weatherDesc = new String[3];
            weatherDesc[0] = weatherObject2.getString("description");
            weatherDesc[1] = weatherObject2.getString("icon");

            JSONObject mainObject = result.getJSONObject("main");
            double kelvin = mainObject.getDouble("temp");
            double celsius = (kelvin - 273.15);
            int celsiusRounded = (int) Math.round(celsius);

            weatherDesc[2] = String.valueOf(celsiusRounded);

            // returns the string array with the information
            return weatherDesc;

        }catch(MalformedURLException mal){
            mal.printStackTrace();
        }
        catch(IOException io){
            io.printStackTrace();
        }
        catch(JSONException j){
            j.printStackTrace();
        }
        finally {
        }
        return null;
    }

    /**
     * This method takes an inputstream and reads it into a string, and returns the string.
     * @param inputStream the inputstream
     * @return String of inputstream
     * @throws IOException
     */
    public String readStream(InputStream inputStream) throws IOException {
        Reader reader;
        reader = new InputStreamReader(inputStream);
        char[] temp = new char[800];
        reader.read(temp);
        return new String(temp);
    }

}
